#!/bin/bash

pushd frontend-stock-portfolio/

./stopFrontend.sh

popd

pushd backend-stock-portfolio/

./gradlew clearContainer
