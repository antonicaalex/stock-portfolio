#!/bin/bash



pushd backend-stock-portfolio/

./gradlew clearContainer
./gradlew start

popd

pushd frontend-stock-portfolio/

./stopFrontend.sh && ./startFrontend.sh 