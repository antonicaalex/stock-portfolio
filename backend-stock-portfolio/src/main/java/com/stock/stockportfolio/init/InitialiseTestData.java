package com.stock.stockportfolio.init;

import com.stock.stockportfolio.dtos.PortfolioDTO;
import com.stock.stockportfolio.dtos.StockDTO;
import com.stock.stockportfolio.services.PortfolioService;
import com.stock.stockportfolio.services.StockService;
import com.stock.stockportfolio.ws.dto.StockWsDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * This file was created by aantonica on 01/11/2020
 */
@Component
@AllArgsConstructor
@Slf4j
public class InitialiseTestData {

    final StockService stockService;
    final PortfolioService portfolioService;

    final String[] testStocks = new String[]{"HSBC", "Apple", "Tata Motors", "Royal Dutch Shell", "Commerzbank AG", "Deutsche Telekom", "Deutsche Lufthansa", "Michelin", "Louis Vuitton", "Munich Re", "Peugeot SA", "Renault ORD", "Siemens AG"};
    final String[] testPortfolios = new String[]{"Portfolio A", "Portfolio B", "Portfolio C", "Portfolio D"};

    List<StockWsDTO> createdStocks;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {

        log.info("Start adding stocks....");
        addStocks();
        log.info("Finished adding stocks");

        log.info("Start adding portfolios");
        addPortfolios();
        log.info("Finished adding portfolios");
    }


    private void addStocks() {
        stockService.deleteAllStocks();

        for (String testStock : testStocks) {
            StockDTO stockDTO = new StockDTO();
            stockDTO.setName(testStock);

            final StockWsDTO stockWsDTO = stockService.createStock(stockDTO);

            createdStocks.add(stockWsDTO);
        }
    }

    private void addPortfolios() {

        portfolioService.deleteAllPortfolios();

        for (String portfolio : testPortfolios) {
            PortfolioDTO portfolioDTO = new PortfolioDTO();
            portfolioDTO.setName(portfolio);
            portfolioDTO.setStocks(createRandomSetOfStocks());
            portfolioService.createPortfolio(portfolioDTO);
        }
    }

    private Map<String, Integer> createRandomSetOfStocks() {

        Random r = new Random();
        int low = 0;
        int high = createdStocks.size();
        int numberOfStocks = r.nextInt(high-low) + low;
        int indexOfStock;
        int numberOfHoldingsPerStock;

        Map<String, Integer> mapStocks = new HashMap<>();

        for (int i = 0; i < numberOfStocks; i++) {
            numberOfHoldingsPerStock = (int) (Math.random() * 100);
            indexOfStock = r.nextInt(high-low) + low;

            final StockWsDTO stockWsDTO = createdStocks.get(indexOfStock);

            mapStocks.put(stockWsDTO.getStockDTO().getName(), numberOfHoldingsPerStock);
        }

        return mapStocks;
    }

}
