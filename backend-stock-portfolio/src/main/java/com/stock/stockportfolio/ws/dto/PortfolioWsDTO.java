package com.stock.stockportfolio.ws.dto;

import com.stock.stockportfolio.dtos.PortfolioDTO;
import com.stock.stockportfolio.utils.enums.WsDTOStatusCode;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * This file was created by aantonica on 31/10/2020
 */
@Data
@AllArgsConstructor
public class PortfolioWsDTO {

    private final PortfolioDTO portfolioDTO;
    private final WsDTOStatusCode status;
}
