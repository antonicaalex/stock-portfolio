package com.stock.stockportfolio.ws.dto;

import com.stock.stockportfolio.dtos.PortfolioDTO;
import com.stock.stockportfolio.dtos.StockDTO;
import com.stock.stockportfolio.utils.enums.WsDTOStatusCode;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * This file was created by aantonica on 01/11/2020
 */
@Data
@AllArgsConstructor
public class StockWsDTO {

    private final StockDTO stockDTO;
    private final WsDTOStatusCode status;
}
