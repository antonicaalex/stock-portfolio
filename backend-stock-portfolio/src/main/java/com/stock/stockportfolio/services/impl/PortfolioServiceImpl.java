package com.stock.stockportfolio.services.impl;

import com.stock.stockportfolio.daos.PortfolioDAO;
import com.stock.stockportfolio.dtos.PortfolioDTO;
import com.stock.stockportfolio.exceptions.EntityCrudException;
import com.stock.stockportfolio.models.PortfolioModel;
import com.stock.stockportfolio.services.PortfolioService;
import com.stock.stockportfolio.utils.CustomMapper;
import com.stock.stockportfolio.ws.dto.PortfolioWsDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.stock.stockportfolio.utils.enums.WsDTOStatusCode.*;

/**
 * This file was created by aantonica on 31/10/2020
 */
@Service
@AllArgsConstructor
@Slf4j
public class PortfolioServiceImpl implements PortfolioService {

    private final String COULD_NOT_UPDATE_PORTFOLIO_MESSAGE = "Could not update portfolio";
    private final String COULD_NOT_CREATE_PORTFOLIO_MESSAGE = "Could not create portfolio";

    final PortfolioDAO portfolioDAO;
    final CustomMapper customMapper;

    @Override
    public List<PortfolioDTO> retrieveAllPortfolios() {

        final List<PortfolioModel> portfolioModels = portfolioDAO.findAll();

        return customMapper.mapList(portfolioModels, PortfolioDTO.class);
    }

    @Override
    public PortfolioWsDTO updatePortfolio(final PortfolioDTO portfolioDTO) {

        final PortfolioModel portfolioModel = customMapper.map(portfolioDTO, PortfolioModel.class);
        final PortfolioDTO portfolioDTOFromModel;

        try {
            portfolioDAO.save(portfolioModel);

        } catch (OptimisticLockingFailureException e) {
            return handleOptimisticLockException(portfolioDTO, e);
        } catch (RuntimeException e) {
            throw new EntityCrudException(COULD_NOT_UPDATE_PORTFOLIO_MESSAGE, e);
        }

        portfolioDTOFromModel = customMapper.map(portfolioModel, PortfolioDTO.class);
        return new PortfolioWsDTO(portfolioDTOFromModel, SUCCESS);
    }

    private PortfolioWsDTO handleOptimisticLockException(PortfolioDTO portfolioDTO, OptimisticLockingFailureException e) {
        PortfolioDTO portfolioDTOFromModel;
        final Optional<PortfolioModel> optionalCurrentPortfolioModel = portfolioDAO.findById(portfolioDTO.getId());
        if (optionalCurrentPortfolioModel.isPresent()) {

            final PortfolioModel currentPortfolioModel = optionalCurrentPortfolioModel.get();
            portfolioDTOFromModel = customMapper.map(currentPortfolioModel, PortfolioDTO.class);

            return new PortfolioWsDTO(portfolioDTOFromModel, NEEDS_UPDATE);
        }

        throw new EntityCrudException(COULD_NOT_UPDATE_PORTFOLIO_MESSAGE, e);
    }

    @Override
    public PortfolioWsDTO createPortfolio(PortfolioDTO portfolioDTO) {

        final PortfolioModel portfolioModel = customMapper.map(portfolioDTO, PortfolioModel.class);
        final PortfolioDTO portfolioDTOFromModel;

        try {
            portfolioDAO.save(portfolioModel);
        } catch (RuntimeException e) {
            throw new EntityCrudException(COULD_NOT_CREATE_PORTFOLIO_MESSAGE, e);
        }

        portfolioDTOFromModel = customMapper.map(portfolioModel, PortfolioDTO.class);
        return new PortfolioWsDTO(portfolioDTOFromModel, SUCCESS);
    }

    @Override
    public PortfolioWsDTO deletePortfolio(String id) {

        final Optional<PortfolioModel> optionalCurrentPortfolioModel = portfolioDAO.findById(id);
        if (optionalCurrentPortfolioModel.isPresent()) {

            final PortfolioModel portfolioModel = optionalCurrentPortfolioModel.get();

            try {
                portfolioDAO.delete(portfolioModel);
                return new PortfolioWsDTO(null, SUCCESS);
            } catch (RuntimeException e) {
                throw new EntityCrudException("Could not delete portfolio with id " + id, e);
            }
        }

        throw new EntityCrudException("No such element to delete");
    }

    @Override
    public void deleteAllPortfolios() {

        portfolioDAO.deleteAll();
    }
}
