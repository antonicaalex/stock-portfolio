package com.stock.stockportfolio.services;

import com.stock.stockportfolio.dtos.PortfolioDTO;
import com.stock.stockportfolio.ws.dto.PortfolioWsDTO;

import java.util.List;

/**
 * This file was created by aantonica on 31/10/2020
 */
public interface PortfolioService {

    List<PortfolioDTO> retrieveAllPortfolios();

    PortfolioWsDTO createPortfolio(PortfolioDTO portfolioDTO);

    PortfolioWsDTO updatePortfolio(PortfolioDTO portfolioDTO);

    PortfolioWsDTO deletePortfolio(String id);

    void deleteAllPortfolios();
}
