package com.stock.stockportfolio.services;

import com.stock.stockportfolio.dtos.StockDTO;
import com.stock.stockportfolio.ws.dto.StockWsDTO;

import java.util.List;

/**
 * This file was created by aantonica on 01/11/2020
 */
public interface StockService {

    StockWsDTO updateStock(StockDTO stockDTO);

    StockWsDTO createStock(StockDTO stockDTO);

    List<StockDTO> retrieveAllStocks();

    StockWsDTO deleteStock(String id);

    void deleteAllStocks();

}
