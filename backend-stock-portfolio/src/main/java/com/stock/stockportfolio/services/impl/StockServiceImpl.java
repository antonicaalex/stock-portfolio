package com.stock.stockportfolio.services.impl;

import com.stock.stockportfolio.daos.StockDAO;
import com.stock.stockportfolio.dtos.StockDTO;
import com.stock.stockportfolio.exceptions.EntityCrudException;
import com.stock.stockportfolio.models.StockModel;
import com.stock.stockportfolio.services.StockService;
import com.stock.stockportfolio.utils.CustomMapper;
import com.stock.stockportfolio.ws.dto.StockWsDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.stock.stockportfolio.utils.enums.WsDTOStatusCode.SUCCESS;

/**
 * This file was created by aantonica on 01/11/2020
 */
@Service
@AllArgsConstructor
@Slf4j
public class StockServiceImpl implements StockService {

    private static final String COULD_NOT_CREATE_STOCK_MESSAGE = "Could not create stock";

    private final StockDAO stockDAO;
    private final CustomMapper customMapper;

    @Override
    public StockWsDTO updateStock(StockDTO stockDTO) {
        return null;
    }

    @Override
    public StockWsDTO createStock(StockDTO stockDTO) {

        final StockModel stockModel = customMapper.map(stockDTO, StockModel.class);
        final StockDTO stockDTOFromModel;

        try {
            stockDAO.save(stockModel);
        } catch (RuntimeException e) {
            throw new EntityCrudException(COULD_NOT_CREATE_STOCK_MESSAGE, e);
        }

        stockDTOFromModel = customMapper.map(stockModel, StockDTO.class);
        return new StockWsDTO(stockDTOFromModel, SUCCESS);
    }

    @Override
    public List<StockDTO> retrieveAllStocks() {

        final List<StockModel> stockModels = stockDAO.findAll();

        return customMapper.mapList(stockModels, StockDTO.class);
    }

    @Override
    public StockWsDTO deleteStock(String id) {

        final Optional<StockModel> optionalStockModel = stockDAO.findById(id);
        if (optionalStockModel.isPresent()) {

            final StockModel stockModel = optionalStockModel.get();

            try {
                stockDAO.delete(stockModel);
            } catch (RuntimeException e) {
                throw new EntityCrudException("Could not delete stock with id " + id, e);
            }
        }

        throw new EntityCrudException("No such element to delete");
    }

    @Override
    public void deleteAllStocks() {

        stockDAO.deleteAll();
    }
}
