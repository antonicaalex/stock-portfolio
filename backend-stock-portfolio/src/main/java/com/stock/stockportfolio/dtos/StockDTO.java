package com.stock.stockportfolio.dtos;

import com.stock.stockportfolio.models.GenericModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This file was created by aantonica on 01/11/2020
 */
@Data
@Document(collection = "stocks")
@EqualsAndHashCode(callSuper = true)
public class StockDTO extends GenericModel {

    private String name;

}
