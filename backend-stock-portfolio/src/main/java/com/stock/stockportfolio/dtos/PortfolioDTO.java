package com.stock.stockportfolio.dtos;

import com.stock.stockportfolio.models.StockModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Map;

/**
 * This file was created by aantonica on 31/10/2020
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PortfolioDTO extends GenericDTO {

    private String name;
    private Map<String, Integer> stocks;

}
