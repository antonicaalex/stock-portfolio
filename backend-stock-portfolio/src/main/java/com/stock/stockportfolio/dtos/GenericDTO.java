package com.stock.stockportfolio.dtos;

import lombok.Data;

import java.util.Date;

/**
 * This file was created by aantonica on 31/10/2020
 */
@Data
public class GenericDTO {

    private String id;
    private Date createdAt;
    private Date updatedAt;
    private Long version;

}
