package com.stock.stockportfolio.utils;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This file was created by aantonica on 31/10/2020
 */
@Component
public class CustomMapper extends ModelMapper {

    public CustomMapper() {
        super();

        this.getConfiguration().setSkipNullEnabled(true);
    }

    public <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source.stream().map(item -> map(item, targetClass)).collect(Collectors.toList());
    }

}
