package com.stock.stockportfolio.utils;

import lombok.Getter;

/**
 * This file was created by aantonica on 31/10/2020
 */
@Getter
public class ErrorInfo {
    public final String message;
    public final String ex;

    public ErrorInfo(String message, Exception ex) {
        this.message = message;
        this.ex = ex.getLocalizedMessage();
    }
}
