package com.stock.stockportfolio.utils.enums;

/**
 * This file was created by aantonica on 31/10/2020
 */
public enum WsDTOStatusCode {

    SUCCESS,
    ERROR,
    NEEDS_UPDATE

}
