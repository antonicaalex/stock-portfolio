package com.stock.stockportfolio.controllers.handlers;

import com.stock.stockportfolio.exceptions.EntityCrudException;
import com.stock.stockportfolio.utils.ErrorInfo;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

/**
 * This file was created by aantonica on 31/10/2020
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {


    @ResponseStatus(HttpStatus.BAD_REQUEST)  // 422
    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseBody
    public ErrorInfo handleBadInput(HttpServletRequest req, Exception ex) {

        return new ErrorInfo("Request input nonexistent/bad format", ex);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
    @ExceptionHandler({EntityCrudException.class})
    @ResponseBody
    public ErrorInfo entityCrudException(HttpServletRequest req, Exception ex) {

        return new ErrorInfo("Error while trying to apply crud operation on entity", ex);
    }
}
