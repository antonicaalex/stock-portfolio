package com.stock.stockportfolio.controllers;

import com.stock.stockportfolio.dtos.StockDTO;
import com.stock.stockportfolio.services.StockService;
import com.stock.stockportfolio.ws.dto.StockWsDTO;
import lombok.AllArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * This file was created by aantonica on 01/11/2020
 */
@RequestMapping("/stock")
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class StockController {

    private final StockService stockService;
    
    @GetMapping
    public List<StockDTO> retrieveAllStocks() {

        return stockService.retrieveAllStocks();
    }

    @PostMapping
    public StockWsDTO createStock(@RequestBody StockDTO StockDTO) {

        return stockService.createStock(StockDTO);
    }

    @PutMapping
    public StockWsDTO updateStock(@RequestBody StockDTO stockDTO) {

        Assert.notNull(stockDTO.getId(), "Can not update Stock without id");

        return stockService.updateStock(stockDTO);
    }

    @DeleteMapping("/{id}")
    public StockWsDTO deleteStock(@PathVariable String id) {

        return stockService.deleteStock(id);
    }
}
