package com.stock.stockportfolio.controllers;

import com.stock.stockportfolio.dtos.PortfolioDTO;
import com.stock.stockportfolio.services.PortfolioService;
import com.stock.stockportfolio.ws.dto.PortfolioWsDTO;
import lombok.AllArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * This file was created by aantonica on 31/10/2020
 */

@RequestMapping("/portfolio")
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class PortfolioController {

    final PortfolioService portfolioService;

    @GetMapping
    public List<PortfolioDTO> retrieveAllPortfolios() {

        return portfolioService.retrieveAllPortfolios();
    }

    @PostMapping
    public PortfolioWsDTO createPortfolio(@RequestBody PortfolioDTO portfolioDTO) {

        return portfolioService.createPortfolio(portfolioDTO);
    }

    @PutMapping
    public PortfolioWsDTO updatePortfolio(@RequestBody PortfolioDTO portfolioDTO) {

        Assert.notNull(portfolioDTO.getId(), "Can not update portfolio without id");

        return portfolioService.updatePortfolio(portfolioDTO);
    }

    @DeleteMapping("/{id}")
    public PortfolioWsDTO deletePortfolio(@PathVariable String id) {

        return portfolioService.deletePortfolio(id);
    }
}
