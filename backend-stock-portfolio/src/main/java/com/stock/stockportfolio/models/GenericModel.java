package com.stock.stockportfolio.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import static java.util.Objects.isNull;

/**
 * This file was created by aantonica on 31/10/2020
 */
@Data
public class GenericModel implements Serializable, Persistable<String> {

    @Id
    private String id;

    @LastModifiedDate
    private Date updatedAt;

    @CreatedDate
    private Date createdAt;

    @Version
    private Long version = 0L;

    @Override
    @JsonIgnore
    public boolean isNew() {
        return isNull(this.createdAt);
    }

    public GenericModel() {
        this.id = UUID.randomUUID().toString();
    }
}
