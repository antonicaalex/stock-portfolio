package com.stock.stockportfolio.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This file was created by aantonica on 31/10/2020
 */
@Data
@Document(collection = "stocks")
@EqualsAndHashCode(callSuper = true)
public class StockModel extends GenericModel {

    private String name;

}
