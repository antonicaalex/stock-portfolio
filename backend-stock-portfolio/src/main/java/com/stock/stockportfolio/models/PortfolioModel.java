package com.stock.stockportfolio.models;

import com.mongodb.BasicDBObject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

/**
 * This file was created by aantonica on 31/10/2020
 */
 @Data
 @Document(collection = "portfolios")
 @EqualsAndHashCode(callSuper = true)
public class PortfolioModel extends GenericModel {

    private String name;
    private BasicDBObject stocks;

    public void setStocks(Map<String, Integer> stocks) {
        this.stocks = new BasicDBObject( stocks );
    }
}
