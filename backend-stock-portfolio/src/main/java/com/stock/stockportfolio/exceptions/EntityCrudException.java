package com.stock.stockportfolio.exceptions;

/**
 * This file was created by aantonica on 31/10/2020
 */
public class EntityCrudException extends RuntimeException {


    public EntityCrudException(String message) {
        super(message);
    }

    public EntityCrudException(String message, Exception e) {
        super(message, e);
    }
}
