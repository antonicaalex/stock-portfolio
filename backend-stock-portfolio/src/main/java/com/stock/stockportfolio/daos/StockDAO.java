package com.stock.stockportfolio.daos;

import com.stock.stockportfolio.models.StockModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * This file was created by aantonica on 01/11/2020
 */
 @Repository
public interface StockDAO extends MongoRepository<StockModel, String> {
}
