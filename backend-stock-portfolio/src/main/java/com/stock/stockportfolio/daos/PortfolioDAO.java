package com.stock.stockportfolio.daos;

import com.stock.stockportfolio.models.PortfolioModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * This file was created by aantonica on 31/10/2020
 */
 @Repository
public interface PortfolioDAO extends MongoRepository<PortfolioModel, String> {

}
