#!/bin/bash

# Run docker-compose
docker-compose -f docker-compose.yaml down

# Delete old image
docker rmi stock-portfolio-fe:v1 -f
