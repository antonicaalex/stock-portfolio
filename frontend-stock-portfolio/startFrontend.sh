#!/bin/bash


pushd stock-portfolio-fe

# Build for prod
npm install
ng build --prod --output-hashing=all
rm -rf node_modules/

popd

# Delete old image
docker rmi stock-portfolio-fe:v1 -f

# Create docker image
docker build -t stock-portfolio-fe:v1 .

# Run docker-compose
docker-compose -f docker-compose.yaml up -d stock-portfolio-fe