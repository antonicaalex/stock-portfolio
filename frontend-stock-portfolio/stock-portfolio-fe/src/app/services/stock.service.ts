import { StockModel } from './../models/stock-model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  fetchAllStocks(): Observable<StockModel[]> {

    const url = `${environment.ENDPOINTS.GET_ALL_STOCKS}`;

    return this.httpClient.get<StockModel[]>(url);
  }

  constructor(private httpClient: HttpClient) { }
}
