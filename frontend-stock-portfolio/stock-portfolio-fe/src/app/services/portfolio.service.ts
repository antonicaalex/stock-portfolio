import { StockModel } from './../models/stock-model';
import { PortfoliosModel } from 'src/app/models/portfolio-model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PortfoliosWsDTO } from '../models/portfolio-ws-dto';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PortfolioService {


  private portfolioSubject: BehaviorSubject<PortfoliosModel>;

  constructor(private httpClient: HttpClient,
    private snackBar: MatSnackBar) {

  }

  public get currentPortfolioValue() {
    return this.portfolioSubject.value;
  }

  fetchAllPortfolios(): Observable<PortfoliosModel[]> {

    const url = `${environment.ENDPOINTS.GET_ALL_PORTFOLIOS}`;

    return this.httpClient.get<PortfoliosModel[]>(url);
  }

  updatePortfolio(portfolio: PortfoliosModel): Observable<PortfoliosWsDTO> {

    const url = `${environment.ENDPOINTS.UPDATE_PORTFOLIO}`;

    return this.httpClient.put<PortfoliosWsDTO>(url, portfolio)
      .pipe(map(result => {

        if (result.status === 'NEEDS_UPDATE') {
          this.snackBar.open('Portfolio was already updated by someonoe else. We refreshed it for you. Check again', 'OK', {
            duration: 2000,
          });
          return result;
        }

        return result;
      }
      ));
  }

  updateCurrentPorfolioWithStock(stock: StockModel, value): PortfoliosModel {

    if (this.currentPortfolioValue !== null) {

      const localCopyOfCurrentPortfolio = Object.assign({}, this.currentPortfolioValue);

      localCopyOfCurrentPortfolio.stocks[stock.name] = value;

      return localCopyOfCurrentPortfolio;
    }

    this.snackBar.open('No current portfolio selected', 'OK', {
      duration: 3000,
    });

    return null;
  }

  setCurrentPortfolio(portfolio) {
    if (portfolio === null || portfolio === undefined) {
      this.portfolioSubject = new BehaviorSubject<PortfoliosModel>(null);
    } else {
      this.portfolioSubject = new BehaviorSubject<PortfoliosModel>(portfolio);
    }
  }

  deletePortfolio(id: any) {
    const url = `${environment.ENDPOINTS.DELETE_PORTFOLIO}`;

    return this.httpClient.delete<PortfoliosWsDTO>(url + '/' + id);
  }
}
