import { PortfoliosModel } from 'src/app/models/portfolio-model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'stock-portfolio-fe';

  portfolio: PortfoliosModel;

  handlePortfolioSelection(portfolio) {
    console.log('REceived poortfolio:' + portfolio);
    
    this.portfolio = portfolio;
  }

  handlePortfolioStockUpdate(portfolio) {
    this.portfolio = portfolio;
  }
}
