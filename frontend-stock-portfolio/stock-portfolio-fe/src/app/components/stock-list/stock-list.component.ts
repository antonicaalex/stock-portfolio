import { StockModel } from './../../models/stock-model';
import { EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PortfoliosModel } from '../../models/portfolio-model';
import { PortfolioService } from '../../services/portfolio.service';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StockService } from 'src/app/services/stock.service';

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.scss']
})
export class StockListComponent implements OnInit {

  displayedColumns: string[] = ['name', 'actions'];

  dataSourceArray: StockModel[] = [];
  dataSource = new MatTableDataSource<StockModel>(this.dataSourceArray);
  @ViewChild(MatSort) sort: MatSort;

  @Output()
  portfolioUpdated: EventEmitter<PortfoliosModel> = new EventEmitter<PortfoliosModel>();

  constructor(private portfolioService: PortfolioService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private stockService: StockService) { }

  openDialog(event, stock): void {
    event.stopPropagation();

    console.log(stock.name);

    const dialogRef = this.dialog.open(StockValueAddDialogComponent, {
      width: '250px',
      data: { stock, value: 0}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log('Result:' + result);
      if (result !== undefined && result.value !== undefined && Number.isInteger(+result.value)) {
        console.log("Check something here" + result);
        this.addStockToCurrentPortfolio(result.stock, result.value);
      }
    });
  }

  ngOnInit(): void {

    this.stockService.fetchAllStocks()
      .subscribe(result => {
        this.handleTableRefresh(result);
      });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  handleTableRefresh(result: StockModel[]) {
    this.dataSourceArray = result;
    this.dataSource.data = this.dataSourceArray;
  }

  tableDataChanged() {
    this.dataSource.data = this.dataSourceArray;
  }

  addStockToCurrentPortfolio(stock, value) {

      const tempUpdatedCurrentPortfolio = this.portfolioService.updateCurrentPorfolioWithStock(stock, value);
      if (tempUpdatedCurrentPortfolio !== null) {

      this.portfolioService.updatePortfolio(tempUpdatedCurrentPortfolio)
      .subscribe(
        updatedPortfolioWsDTO => {

          this.portfolioUpdated.emit(updatedPortfolioWsDTO.portfolioDTO);
          this.portfolioService.setCurrentPortfolio(updatedPortfolioWsDTO.portfolioDTO);
        },
        error => {
          this.snackBar.open(error.error.ex, 'OK', {
            duration: 2000,
          });
        });
    } else {
      this.snackBar.open('No current portfolio selected', 'OK', {
        duration: 3000,
      });
    }
  }
}


// ----------------

export interface DialogValue {
  stock: StockModel;
  value: number;
}

@Component({
  selector: 'app-name-change-dialog',
  templateUrl: '../../commons/stock-value-dialog/stock-value-dialog.component.html',
  styleUrls: ['../../commons/stock-value-dialog/stock-value-dialog.component.scss']
})
export class StockValueAddDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<StockListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogValue) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
