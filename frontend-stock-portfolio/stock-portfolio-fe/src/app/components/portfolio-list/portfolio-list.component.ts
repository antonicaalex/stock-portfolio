import { EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { PortfoliosModel } from '../../models/portfolio-model';
import { PortfolioService } from '../../services/portfolio.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-portfolio-list',
  templateUrl: './portfolio-list.component.html',
  styleUrls: ['./portfolio-list.component.scss']
})
export class PortfolioListComponent implements OnInit {
  displayedColumns: string[] = ['name', 'actions'];

  private _portfolioUpdated: PortfoliosModel;

  dataSourceArray: PortfoliosModel[] = [];
  dataSource = new MatTableDataSource<PortfoliosModel>(this.dataSourceArray);
  @ViewChild(MatSort) sort: MatSort;

  @Output()
  portfolioSelected: EventEmitter<PortfoliosModel> = new EventEmitter<PortfoliosModel>();

  @Input()
  set portfolioUpdated(value: PortfoliosModel) {
    this.handlePortfolioUpdate(value);
  }


  get portfolioUpdated(): PortfoliosModel {
    return this._portfolioUpdated;
  }

  constructor(private portfolioService: PortfolioService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) { }

  openDialog(event, portfolio): void {
    event.stopPropagation();

    console.log(portfolio.name);

    const dialogRef = this.dialog.open(PortfolioNameChangeDialogComponent, {
      width: '250px',
      data: portfolio.name
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log('Result:' + result);
      if (result !== undefined) {
        portfolio.name = result;
        this.updatePortfolio(portfolio);
      }
    });
  }

  ngOnInit(): void {
    this.portfolioService.setCurrentPortfolio(null);

    this.portfolioService.fetchAllPortfolios()
      .subscribe(result => {
        this.handleTableRefresh(result);

        if (result.length > 0) {
          this.rowClick(this.dataSource.data[0]);
        }
      });
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  deletePortfolio(row) {
    console.log(row);


    this.portfolioService.deletePortfolio(row.id)
      .subscribe(
        result => {

          this.handlePortfolioDelete(row.id);
        },
        error => {
          this.snackBar.open(error.error.ex, 'OK', {
            duration: 2000,
          });
        });

  }

  handlePortfolioDelete(id: string) {

    if (this.portfolioService.currentPortfolioValue.id == id) {
      this.portfolioService.setCurrentPortfolio(null);
    }

    this.dataSourceArray = this.dataSourceArray.filter(portfolioItem => portfolioItem.id !== id);

    this.tableDataChanged();

    this.rowClick(this.dataSource.data[0]);
  }

  handleTableRefresh(result: PortfoliosModel[]) {
    this.dataSourceArray = result;
    this.dataSource.data = this.dataSourceArray;
  }

  tableDataChanged() {
    this.dataSource.data = this.dataSourceArray;
  }

  rowClick(rowData) {
    console.log('Emitting event ' + rowData);

    this.portfolioService.setCurrentPortfolio(rowData);
    this.portfolioSelected.emit(rowData);
  }

  updatePortfolio(portfolio) {

    this.portfolioService.updatePortfolio(portfolio)
      .subscribe(
        updatedPortfolioWsDTO => {

          this.handlePortfolioUpdate(updatedPortfolioWsDTO.portfolioDTO);
        },
        error => {
          this.snackBar.open(error.error.ex, 'OK', {
            duration: 2000,
          });
        });
  }

  private handlePortfolioUpdate(updatedPortfolio: PortfoliosModel) {

    this.portfolioService.setCurrentPortfolio(updatedPortfolio);

    let cuurrentPortfolio = this.dataSourceArray.find(portfolioItem => portfolioItem.id === updatedPortfolio.id);
    const index = this.dataSourceArray.indexOf(cuurrentPortfolio);

    if (cuurrentPortfolio !== undefined) {
      this.dataSourceArray[index] = updatedPortfolio;
    }

    this.tableDataChanged();
  }
}


// ----------------

@Component({
  selector: 'app-name-change-dialog',
  templateUrl: '../../commons/name-change-dialog/name-change-dialog.component.html',
  styleUrls: ['../../commons/name-change-dialog/name-change-dialog.component.scss']
})
export class PortfolioNameChangeDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<PortfolioListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
