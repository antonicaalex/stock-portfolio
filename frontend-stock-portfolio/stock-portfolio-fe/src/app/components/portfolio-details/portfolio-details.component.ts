import { PortfolioService } from './../../services/portfolio.service';
import { PortfoliosModel } from 'src/app/models/portfolio-model';
import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface StockWithValue {
  name: string;
  value: number;
}

@Component({
  selector: 'app-portfolio-details',
  templateUrl: './portfolio-details.component.html',
  styleUrls: ['./portfolio-details.component.scss']
})
export class PortfolioDetailsComponent implements OnInit {

  displayedColumns: string[] = ['name', 'value', 'actions'];
  dataSourceArray: StockWithValue[] = [];

  dataSource = new MatTableDataSource<StockWithValue>(this.dataSourceArray);

  @Output()
  portfolioUpdated: EventEmitter<PortfoliosModel> = new EventEmitter<PortfoliosModel>();


  @ViewChild(MatSort) sort: MatSort;
  private _portfolio: PortfoliosModel;

  @Input()
  set portfolio(portfo: PortfoliosModel) {
    this._portfolio = portfo;

    if (portfo !== undefined) {
      const stockList = Object.keys(portfo.stocks).map(key => ({ name: key, value: portfo.stocks[key] }));

      this.dataSource.data = stockList;
    }
  }


  constructor(public dialog: MatDialog,
              private portfolioService: PortfolioService,
              private snackBar: MatSnackBar) { }

  openDialog(row: StockWithValue): void {
    const dialogRef = this.dialog.open(PortfolioStockValueAddDialogComponent, {
      width: '250px',
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.isValidStockNumber(result)) {
        this.updateStockForCurrentPortfolio(result);
      }
    });
  }

  isValidStockNumber(result) {
    return result !== undefined && result.value !== undefined && Number.isInteger(+result.value);
  }

  updateStockForCurrentPortfolio(result: StockWithValue) {
    console.log('Will try to update stock from details');

    this._portfolio.stocks[result.name] = result.value;

    this.handlePortfolioStockUpdate();
  }

  private handlePortfolioStockUpdate() {
    this.portfolioService.updatePortfolio(this._portfolio)
      .subscribe(
        updatedPortfolioWsDTO => {

          this._portfolio = updatedPortfolioWsDTO.portfolioDTO;
          this.portfolioUpdated.emit(this._portfolio);
        },
        error => {
          this.snackBar.open(error.error.ex, 'OK', {
            duration: 2000,
          });
        });
  }

  ngOnInit(): void {
  }

  deleteStock(row: StockWithValue) {

    delete this._portfolio.stocks[row.name];

    this.handlePortfolioStockUpdate();
  }

}


// // ----------------

@Component({
  selector: 'app-name-change-dialog',
  templateUrl: '../../commons/stock-value-dialog/stock-value-dialog.component.html',
  styleUrls: ['../../commons/stock-value-dialog/stock-value-dialog.component.scss']
})
export class PortfolioStockValueAddDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<PortfolioDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: StockWithValue) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}


