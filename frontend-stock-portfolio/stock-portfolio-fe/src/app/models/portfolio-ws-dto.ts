import { PortfoliosModel } from './portfolio-model';


export interface PortfoliosWsDTO {
    status: string;
    portfolioDTO: PortfoliosModel;
}