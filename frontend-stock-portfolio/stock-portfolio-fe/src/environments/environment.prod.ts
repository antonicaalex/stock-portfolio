// const HOST = 'stock';
const HOST = 'localhost';
const PORT = '9999';
const DOMAIN = `http://${HOST}:${PORT}`;

export const environment = {
  production: true,
  ENDPOINTS: {
    GET_ALL_PORTFOLIOS: `${DOMAIN}/portfolio`,
    UPDATE_PORTFOLIO: `${DOMAIN}/portfolio`,
    GET_ALL_STOCKS: `${DOMAIN}/stock`,
    DELETE_PORTFOLIO: `${DOMAIN}/portfolio`
  }
};


